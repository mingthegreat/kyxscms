<?php
// +----------------------------------------------------------------------
// | StringFilter 敏感词过滤
// +----------------------------------------------------------------------
// | Copyright
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Tealun
// +----------------------------------------------------------------------

namespace app\common\behavior;
use Generator;
use think\Db;
use Yurun\Util\Chinese;
use Yurun\Util\Chinese\Pinyin;

/**
 * 敏感词过滤
 */
class StringFilter
{
    private static $instance = null;
    /**
     * 替换符号
     * @var string
     */
    private static $replaceSymbol = "*";
    /**
     * 敏感词树
     * @var array
     */
    private static $sensitiveWordTree = [];
    private function __construct(){}
    /**
     * 获取实例
     */
    public static function getInstance()
    {
        if (!(self::$instance instanceof StringFilter)) {
            return self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * 添加敏感词，组成树结构。
     * 例如敏感词为：傻子是傻帽，白痴，傻蛋 这组词组成如下结构。
     * [
     *     [傻] => [
     *           [子]=>[
     *               [是]=>[
     *                  [傻]=>[
     *                      [帽]=>[false]
     *                  ]
     *              ]
     *          ],
     *          [蛋]=>[false]
     *      ],
     *      [白]=>[
     *          [痴]=>[false]
     *      ]
     *  ]
     * @param string $file_path 敏感词库文件路径
     */
    public static function addSensitiveWords(string $file_path) :void
    {
        foreach (self::readFile($file_path) as $words) {
            $len = mb_strlen($words);
            $treeArr = &self::$sensitiveWordTree;
            for ($i = 0; $i < $len; $i++) {
                $word = mb_substr($words, $i, 1);
                //敏感词树结尾记录状态为false；
                $treeArr = &$treeArr[$word] ?? $treeArr = false;
            }
        }
    }

    /**
     * 执行过滤
     * @param string $txt
     * @return string
     */
    public static function execFilter(string $txt)
    {

        //获取敏感词替换类型配置
        $filterType = Db::name("Config")
            ->where('name','=','string_filter_type')
            ->value('value');

        //不设置过滤规则则不过滤
        if(!$filterType) return $txt;

        //根据后台设置的类型替换不同的形式，0为全部转为* 1为在字符中增加随机符号 2为转为汉语拼音
        $wordList = self::searchWords($txt);
        if(empty($wordList))
            return $txt;

        //插入字符时随机出字符 默认和拼音为空
        $w = ['#','%','^','&','*','~','|','/'];
        $split = $filterType == 2?$w[array_rand($w)]:'';

        //执行替换
        foreach ($wordList as $key=>$value){
            $wordList[$key] = '';
            $arr = [];
            switch ($filterType){
                case 2:
                    $arr = preg_split('//u', $key, 0, PREG_SPLIT_NO_EMPTY);//分割字符串为单字数组
                    break;
                case 3:
                    $pinYin = Chinese::toPinyin($key, Pinyin::CONVERT_MODE_PINYIN_SOUND);//转换拼音
                    $arr = $pinYin['pinyinSound'][0];//提取转换拼音后的结果字符数组
                    break;
                default:
                    $wordList[$key] = $value;
            }

            //拼接字符数组为字符串
            for($i=0;$i<count($arr);$i++) {
                //增加最后一个不加分隔符
                $in = $i == count($arr)-1?'':$split;
                $wordList[$key] .= $arr[$i].$in;
            }
        }

        return strtr($txt,$wordList);

    }

    /**
     * 搜索敏感词
     * @param string $txt
     * @return array
     */
    private static function searchWords(string $txt) :array
    {
        $txtLength = mb_strlen($txt);
        $wordList = [];
        for($i = 0; $i < $txtLength; $i++){
            //检查字符是否存在敏感词树内,传入检查文本、搜索开始位置、文本长度
            $len = self::checkWordTree($txt,$i,$txtLength);
            //存在敏感词，进行字符替换。
            if($len > 0){
                //搜索出来的敏感词
                $word = mb_substr($txt,$i,$len);
                $wordList[$word] = str_repeat(self::$replaceSymbol,$len);
            }
        }
        return $wordList;
    }

    /**
     * 检查敏感词树是否合法
     * @param string $txt 检查文本
     * @param int $index 搜索文本位置索引
     * @param int $txtLength 文本长度
     * @return int 返回不合法字符个数
     */
    private static function checkWordTree(string $txt, int $index, int $txtLength) :int
    {
        $treeArr = &self::$sensitiveWordTree;
        $wordLength = 0;//敏感字符个数
        $flag = false;
        for($i = $index; $i < $txtLength; $i++){
            $txtWord = mb_substr($txt,$i,1); //截取需要检测的文本，和词库进行比对
            //如果搜索字不存在词库中直接停止循环。
            if(!isset($treeArr[$txtWord])) break;
            if($treeArr[$txtWord] !== false){//检测还未到底
                $treeArr = &$treeArr[$txtWord]; //继续搜索下一层tree
            }else{
                $flag = true;
            }
            $wordLength++;
        }
        //没有检测到敏感词，初始化字符长度
        $flag ?: $wordLength = 0;
        return $wordLength;
    }


    /**
     * 读取文件内容
     * @param string $file_path
     * @return Generator
     */
    private static function readFile(string $file_path) :Generator
    {
        $handle = fopen($file_path, 'r');
        while (!feof($handle)) {
            yield trim(fgets($handle));
        }
        fclose($handle);
    }

    private function __clone()
    {
        throw new \Exception("clone instance failed!");
    }

    private function __wakeup()
    {
        throw new \Exception("unserialize instance failed!");
    }

}